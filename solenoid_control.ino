#define ledPin 13
volatile unsigned preload_ticks;

int preload(unsigned mSec) {
  /* Assumes a prescaler of 256.

      One millisecond takes 62.5  ticks. We'll compute the number of ticks by multiplying the desired
     number of milliseconds by 125 and then divide by 2.
     1/(16000000/256) = 256 / 16000000 = 1.6e-5 (seconds per tick)
     .001 seconds / (1.6e-5 seconds/tick) -> 62.5 ticks / 0.001 sec
  */

  unsigned ticks_required = unsigned((mSec * 125L) >> 1);
  return 65535 - ticks_required + 1;
}
void setup()
{
  preload_ticks = preload(46);
  pinMode(ledPin, OUTPUT);

  // initialize timer1
  noInterrupts();           // disable all interrupts
  TCCR1A = 0;
  TCCR1B = 0;

  TCNT1 = preload_ticks;
  TCCR1B |= (1 << CS12);    // 256 prescaler
  TIMSK1 |= (1 << TOIE1);   // enable timer overflow interrupt
  interrupts();             // enable all interrupts
  Serial.begin(115200);
}

ISR(TIMER1_OVF_vect)        // interrupt service routine that wraps a user defined function supplied by attachInterrupt
{
  TCNT1 = preload_ticks;            // preload timer
  digitalWrite(ledPin, digitalRead(ledPin) ^ 1);
}


void loop()
{
  /*
      Reads two bytes from serial, interprets them as unsigned int and updates preload_ticks
  */

  unsigned mSec;
  int incoming;
  if (Serial.available() > 1) {
    // read the incoming byte:
    incoming = Serial.read();
    mSec = (incoming << 8) | Serial.read();
    if (mSec > 1000) {
      mSec = 1000;
    }
    preload_ticks = preload(mSec);
  }

}


